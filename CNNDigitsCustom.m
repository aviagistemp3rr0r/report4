%% Create Simple Deep Learning Network for Classification
digitDatasetPath = fullfile(matlabroot,'toolbox','nnet','nndemos', ...
        'nndatasets','DigitDataset');
digitData = imageDatastore(digitDatasetPath, ...
        'IncludeSubfolders',true,'LabelSource','foldernames');

CountLabel = digitData.countEachLabel;
img = readimage(digitData,1);
size(img)
trainingNumFiles = 750;
rng(1) % For reproducibility
[trainDigitData,testDigitData] = splitEachLabel(digitData, ...
				trainingNumFiles,'randomize'); 

% Loop epochs
setMaxEpochs = 30; %15

layers = [imageInputLayer([28 28 1])
  convolution2dLayer(5,20)
  reluLayer
  maxPooling2dLayer(2,'Stride',2)
  fullyConnectedLayer(10)
  softmaxLayer
  classificationLayer()];  %+-3min

layers2 = [
    imageInputLayer([28 28 1])  
    convolution2dLayer(3,16,'Padding',1)
    reluLayer    
    maxPooling2dLayer(2,'Stride',2) 
    convolution2dLayer(3,32,'Padding',1)
    reluLayer 
    fullyConnectedLayer(10)
    softmaxLayer
    classificationLayer]; 

layers3 = [
    imageInputLayer([28 28 1])  
    convolution2dLayer(3,16,'Padding',1)
    batchNormalizationLayer
    reluLayer    
    maxPooling2dLayer(2,'Stride',2) 
    convolution2dLayer(3,32,'Padding',1)
    batchNormalizationLayer
    reluLayer 
    fullyConnectedLayer(10)
    softmaxLayer
    classificationLayer];  

layers5 = [imageInputLayer([28 28 1])
  convolution2dLayer(9,12)
  batchNormalizationLayer
  reluLayer
  maxPooling2dLayer(2,'Stride',2)  
  convolution2dLayer(9,24)
  batchNormalizationLayer
  reluLayer    
  fullyConnectedLayer(10)
  softmaxLayer
  classificationLayer()]; %+-10min

layers6 = [imageInputLayer([28 28 1])
  convolution2dLayer(9,12)
  batchNormalizationLayer
  reluLayer
  maxPooling2dLayer(2,'Stride',2)  
  convolution2dLayer(9,24)
  batchNormalizationLayer
  reluLayer  
  
  convolution2dLayer(2,24)
  batchNormalizationLayer
  reluLayer  
  
  fullyConnectedLayer(10)
  softmaxLayer
  classificationLayer()]; %+-10min
      
options = trainingOptions('sgdm','MaxEpochs',setMaxEpochs, ...
	'InitialLearnRate',0.0001,'OutputFcn',@plotTrainingAccuracy);  

tic
convnet = trainNetwork(trainDigitData,layers,options);
convnet2 = trainNetwork(trainDigitData,layers2,options);
convnet3 = trainNetwork(trainDigitData,layers3,options);
convnet5 = trainNetwork(trainDigitData,layers5,options);
convnet6 = trainNetwork(trainDigitData,layers6,options);
toc

YTest = classify(convnet,testDigitData);
TTest = testDigitData.Labels;

accuracy = sum(YTest == TTest)/numel(TTest);   
accuracy2 = sum(classify(convnet2,testDigitData) == TTest)/numel(TTest);
accuracy3 = sum(classify(convnet3,testDigitData) == TTest)/numel(TTest); 
accuracy5 = sum(classify(convnet5,testDigitData) == TTest)/numel(TTest);
accuracy6 = sum(classify(convnet6,testDigitData) == TTest)/numel(TTest);  

function plotTrainingAccuracy(info)
    persistent plotObj
    setColours = {'r', 'g', 'b', 'k', 'm', 'c', 'y', [1 0.4 0.6], [0.4 1.0 0.6], [0.6 0.4 1.0]};
    if info.State == "start"
        plotObj = animatedline('Color',setColours{randi([1 10])});
        title('Class Accuracy: Convolutional Neural Networks (digits 0-9)');
        xlabel("Iteration")
        ylabel("Training Accuracy")
        legend({'1x Conv (size: 5)', ...
            '2x Conv (size: 3)','2x Conv norm (size: 3)', ...
            '2x Conv norm (size: 9)', ...
            '3x Conv norm (size: 9)'}, 'Location', 'southeast')
    elseif info.State == "iteration"
        addpoints(plotObj,info.Iteration,info.TrainingAccuracy)
        drawnow limitrate nocallbacks
    end
end