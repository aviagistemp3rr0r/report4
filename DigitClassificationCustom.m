clear all; close all; clc;
nntraintool('close');
nnet.guis.closeAllViews();

rng('default')
load('digittrain_dataset');

% Params
hiddenSize1 = 100; % 100;
hiddenSize2 = 50; % 50;
hiddenSize3 = 20; % 20;
maxEpochs1 = 20; % 400;
maxEpochs2 = 10; % 100;
maxEpochs3 = 10; % 100;
maxEpochsOutput = 10; % 400;

%setMaxEpochsOutput = [1, 4, 10];
setMaxEpochsOutput = 1:50:400;
classAccuraciesPlot = zeros(5, length(setMaxEpochsOutput));

for k=1:length(setMaxEpochsOutput)
    
    maxEpochsOutput = setMaxEpochsOutput(k);
    maxEpochs1 = maxEpochsOutput;
    maxEpochs2 = maxEpochsOutput;
    maxEpochs3 = maxEpochsOutput;
    
    % Layer 1
    autoenc1 = trainAutoencoder(xTrainImages,hiddenSize1, ...
        'MaxEpochs',maxEpochs1, ...
        'L2WeightRegularization',0.004, ...
        'SparsityRegularization',4, ...
        'SparsityProportion',0.15, ...
        'ScaleData', false, ...
        'ShowProgressWindow', false);
    feat1 = encode(autoenc1,xTrainImages);

    % Layer 2
    autoenc2 = trainAutoencoder(feat1,hiddenSize2, ...
        'MaxEpochs',maxEpochs2, ...
        'L2WeightRegularization',0.002, ...
        'SparsityRegularization',4, ...
        'SparsityProportion',0.1, ...
        'ScaleData', false, ...
        'ShowProgressWindow', false);
    feat2 = encode(autoenc2,feat1);
    
    % Layer 3
    autoenc3 = trainAutoencoder(feat2,hiddenSize3, ...
        'MaxEpochs',maxEpochs3, ...
        'L2WeightRegularization',0.002, ...
        'SparsityRegularization',4, ...
        'SparsityProportion',0.1, ...
        'ScaleData', false, ...
        'ShowProgressWindow', false);
    feat3 = encode(autoenc3,feat2);

    % Output Layers
    deepnet = stack(autoenc1,autoenc2,trainSoftmaxLayer(feat2,tTrain,'MaxEpochs',maxEpochsOutput, 'ShowProgressWindow', false)); % Deep Net
    deepnet3 = stack(autoenc1,autoenc2,autoenc3,trainSoftmaxLayer(feat3,tTrain,'MaxEpochs',maxEpochsOutput, 'ShowProgressWindow', false)); % Deep Net 3

    % Test deep net
    imageWidth = 28;
    imageHeight = 28;
    inputSize = imageWidth*imageHeight;
    %[xTestImages, tTest] = digittest_dataset;
    load('digittest_dataset');
    xTest = zeros(inputSize,numel(xTestImages));
    for i = 1:numel(xTestImages)
        xTest(:,i) = xTestImages{i}(:);
    end
    y = deepnet(xTest);
    y3 = deepnet3(xTest);
    %figure;
    %plotconfusion(tTest,y);
    stackedClassAcc=100*(1-confusion(tTest,y));
    stacked3ClassAcc=100*(1-confusion(tTest,y3));

    % Test fine-tuned deep net
    xTrain = zeros(inputSize,numel(xTrainImages));
    for i = 1:numel(xTrainImages)
        xTrain(:,i) = xTrainImages{i}(:);
    end
    deepnet = train(deepnet,xTrain,tTrain);
    deepnet3 = train(deepnet3,xTrain,tTrain);
    y = deepnet(xTest);
    y3 = deepnet3(xTest);
    stackedTunedClassAcc=100*(1-confusion(tTest,y));
    stacked3TunedClassAcc=100*(1-confusion(tTest,y3));

    %Compare with normal neural network (1 hidden layers)

    % Params
    net1.trainParam.epochs=maxEpochsOutput;

    net = patternnet(100);
    net.trainParam.showWindow = 0;
    net=train(net,xTrain,tTrain);
    y=net(xTest);
    mlpClassAcc=100*(1-confusion(tTest,y));
    
    classAccuraciesPlot(1, k) = stackedClassAcc;
    classAccuraciesPlot(2, k) = stackedTunedClassAcc;
    classAccuraciesPlot(3, k) = stacked3ClassAcc;
    classAccuraciesPlot(4, k) = stacked3TunedClassAcc;
    classAccuraciesPlot(5, k) = mlpClassAcc;    
end

figure;
plot(classAccuraciesPlot');
title('Class Accuracy: Stacked Autoencoder vs MLP (digits 0-9)');
legend({'Stacked:2 layer','StackedTuned:2 layer','Stacked:3 layer','StackedTuned:3 layer','MLP:1 layer'}, 'Location', 'southeast');
xlabel('Epochs * 50 (all layers)');
ylabel('Class Accuracy %');

% TODO: Compare with normal neural network (2 hidden layers)